package com.redis.user.controller;

import com.redis.user.domain.User;
import com.redis.user.repository.UserRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.Map;

@RestController
public class UserController {
    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/users")
    public Map<String, User> findAll(){
        return userRepository.findAll();
    }

    @GetMapping("/users/{id}")
    public User findById(@PathVariable String id) {
        return userRepository.findById(id);
    }

    @PostMapping("/users")
    public void createUser(@RequestBody User user){
        userRepository.save(user);
    }

    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable String id){
        userRepository.delete(id);
    }

}
