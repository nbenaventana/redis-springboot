package com.redis.user.repository;

import com.redis.user.domain.User;

import java.util.Map;

public interface RedisRepository {
    Map<String, User> findAll();
    User findById(String id);
    void save(User user);
    void delete(String id);
}
